from django.contrib import admin
from django.urls import path

from main.views import IndexView, PageNotFondView
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include

urlpatterns = [
    path("grappelli/", include("grappelli.urls")),  # grappelli URLS
    path("admin/", admin.site.urls),  # admin site
    path("__debug__/", include("debug_toolbar.urls")),
    path("", IndexView.as_view(), name="index"),
    path("main/", include("main.urls")),
    path("students/", include("students.urls")),
    path("teachers/", include("teachers.urls")),
    path("groups/", include("groups.urls")),
    path("oauth/", include("social_django.urls", namespace="social")),
]
handler404 = PageNotFondView.as_view()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
