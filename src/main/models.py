from django.contrib.auth.base_user import AbstractBaseUser
from django.core.validators import MinLengthValidator
from django.db import models
import datetime
from django.utils import timezone

from main.managers import CustomerManager
from main.utils.validators import first_name_validator
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


class Customer(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    email = models.EmailField(_("email address"), unique=True, error_messages="A user with that email already exists.")
    avatar = models.ImageField(upload_to="user/avatar_user", null=True, blank=True)
    phone_number = PhoneNumberField(blank=True, null=True, help_text="+1234567890")
    CHOICES = (
        ("STUDENT", "Student"),
        ("TEACHER", "Teacher"),
        ("MENTOR", "Mentor"),
    )
    user_type = models.CharField(
        max_length=10,
        choices=CHOICES,
    )
    company = models.CharField(blank=True, null=True, max_length=100)
    higher_education = models.BooleanField(default=False, help_text="Check the box if available")
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    objects = CustomerManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("Customer")
        verbose_name_plural = _("All Customers")
        abstract = False
        ordering = ("last_name",)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} {self.email}"

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def get_period_of_registration(self):
        return f"Time on site: {timezone.now() - self.date_joined}"


class Person(models.Model):
    first_name = models.CharField(
        max_length=120,
        null=True,
        blank=True,
        validators=[MinLengthValidator(2, message="incorrect"), first_name_validator],
    )
    last_name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True

    def calculate_age(self):
        if self.birth_date:
            today = datetime.date.today()
            age = today.year - self.birth_date.year
            if today.month < self.birth_date.month or (
                today.month == self.birth_date.month and today.day < self.birth_date.day
            ):
                age -= 1
            return age
        return None
