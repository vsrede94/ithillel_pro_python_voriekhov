from django.contrib.auth import login, get_user_model
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import CreateView, RedirectView, DetailView, UpdateView
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from main.forms import UserRegistrationForm, UserUpdateForm
from main.models import Customer
from main.services.emails import send_registration_email
from main.utils.token_generator import TokenGenerator


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ["get"]


class UserLoginView(LoginView):
    template_name = "login.html"


class UserLogoutView(LogoutView):
    ...


class UserRegistrationView(CreateView):
    template_name = "user_create.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()

        send_registration_email(request=self.request, user_instance=user)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class UserActivateView(RedirectView):
    url = reverse_lazy("index")
    template_name = "emails/wrong_data.html"

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return render(request, self.template_name)

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(request, current_user, backend="django.contrib.auth.backends.ModelBackend")

            return super().get(request, *args, **kwargs)
        return render(request, self.template_name)


class PageNotFondView(TemplateView):
    template_name = "404.html"
    http_method_names = ["get"]


class GuestView(TemplateView):
    template_name = "guest_view.html"
    http_method_names = ["get"]


class ProfileView(DetailView):
    model = Customer
    template_name = "profile.html"
    context_object_name = "user_info"
    queryset = Customer.objects.all()


class UpdateUserView(LoginRequiredMixin, UpdateView):
    model = Customer
    template_name = "user_update.html"
    login_url = "main:guest"
    form_class = UserUpdateForm

    def get_success_url(self):
        return reverse_lazy("main:profile", kwargs={"pk": self.object.pk})
