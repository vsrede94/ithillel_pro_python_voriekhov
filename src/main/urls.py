from django.urls import path

from main.views import (
    UserLoginView,
    UserLogoutView,
    UserRegistrationView,
    GuestView,
    UserActivateView,
    ProfileView,
    UpdateUserView,
)

app_name = "main"

urlpatterns = [
    path("login/", UserLoginView.as_view(), name="login"),
    path("logout/", UserLogoutView.as_view(), name="logout"),
    path("registration/", UserRegistrationView.as_view(), name="registration"),
    path("guest/", GuestView.as_view(), name="guest"),
    path("activate_user/<str:uuid64>/<str:token>/", UserActivateView.as_view(), name="activate_user"),
    path("profile/<int:pk>/", ProfileView.as_view(), name="profile"),
    path("update_user/<int:pk>/", UpdateUserView.as_view(), name="update_user"),
]
