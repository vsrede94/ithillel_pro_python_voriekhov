def cleanup_social_account(backend, uid, user=None, *args, **kwargs):
    if user and "picture" in kwargs.get("response", {}):
        user.avatar = kwargs["response"]["picture"]
        user.save()

    return {"user": user}
