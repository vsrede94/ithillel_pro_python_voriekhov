from django.contrib import admin
from django.contrib.auth import get_user_model


@admin.register(get_user_model())
class CustomerAdmin(admin.ModelAdmin):
    ordering = ("first_name",)
    list_display = ("email", "first_name", "last_name", "phone_number")
    list_display_links = ("email", "first_name")
    search_fields = ("first_name", "last_name")
    fieldsets = None
    readonly_fields = ("email",)
    list_per_page = 20
