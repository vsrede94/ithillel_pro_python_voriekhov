from django.urls import path
from teachers.views import GetTeacherView, CreateTeacherView, UpdateTeacherView, DeleteTeacherView

app_name = "teachers"

urlpatterns = [
    path("", GetTeacherView.as_view(), name="get_teacher"),
    path("create/", CreateTeacherView.as_view(), name="create_teacher"),
    path("update/<int:pk>/", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete/<int:pk>/", DeleteTeacherView.as_view(), name="delete_teacher"),
]
