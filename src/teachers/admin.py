from django.contrib import admin
from django.urls import reverse
from teachers.models import Teacher
from django.utils.html import format_html


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    ordering = ("first_name",)
    list_display = ("email", "first_name", "last_name", "calculate_age", "group_count", "links_to_groups")
    list_display_links = ("email", "first_name")
    list_filter = ("group",)
    date_hierarchy = "birth_date"
    search_fields = ("first_name", "last_name")
    fieldsets = (
        ("Personal info", {"fields": ("email", "first_name", "last_name")}),
        ("Additional info", {"classes": ("collapse",),
                             "fields": ("date_now_add", "current_country", "avatar", "group")})
    )
    list_per_page = 20

    def group_count(self, instance):
        if instance.group:
            return instance.group.count()
        return 0

    def links_to_groups(self, instance):
        if instance.group.exists():
            links = []
            for group in instance.group.all():
                url = reverse('admin:groups_group_change', args=[group.pk])
                link = f'<a class="button" href="{url}">{group.groups_name}</a>'
                links.append(link)
            return format_html("<br><br>".join(links))
        return 'No groups'
