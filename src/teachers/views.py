from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.views.generic import UpdateView, ListView, DeleteView, CreateView

from teachers.forms import TeacherForm
from teachers.models import Teacher
from django.urls import reverse_lazy


class GetTeacherView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teacher_get.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teacher")
    context_object_name = "teachers"
    login_url = "main:guest"

    def get_queryset(self):
        queryset = Teacher.objects.all()
        search = self.request.GET.get("searchField")
        search_fields = ["first_name", "last_name"]
        if search:
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": search})
            queryset = queryset.filter(or_filter)
        print(queryset)
        return queryset


class CreateTeacherView(LoginRequiredMixin, CreateView):
    template_name = "student-create.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teacher")
    login_url = "main:guest"


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    template_name = "teacher_update.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teacher")
    queryset = Teacher.objects.all()
    login_url = "main:guest"


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    model = Teacher
    template_name = "teacher_delete.html"
    success_url = reverse_lazy("teachers:get_teacher")
    login_url = "main:guest"
