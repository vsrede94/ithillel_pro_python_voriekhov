from django.db import models
from faker import Faker

from main.models import Person


class Teacher(Person):
    date_now_add = models.DateField(null=True, blank=True)
    current_country = models.CharField(max_length=30, null=True, blank=True)
    group = models.ManyToManyField(to="groups.Group")
    avatar = models.ImageField(upload_to="teacher/avatar_teacher/", null=True, blank=True)

    class Meta:
        verbose_name = "Teachers"
        verbose_name_plural = "All teachers"
        ordering = ("last_name",)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=18, maximum_age=60),
                date_now_add=faker.date_this_century(),
                current_country=faker.country(),
            )

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} {self.email}"
