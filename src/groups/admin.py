from django.contrib import admin

from groups.models import Group


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    ordering = ("groups_name",)
    list_display = ("groups_name", "direction", "location_school", "price", "get_teachers_list")
    list_display_links = ("groups_name",)
    list_filter = ("groups_name",)
    search_fields = ("location_school",)
    list_per_page = 20

    def get_teachers_list(self, obj):
        teachers_list = obj.teacher_set.all()
        return ", ".join([f"{teacher.first_name} {teacher.last_name}" for teacher in teachers_list])

    get_teachers_list.short_description = "Teachers"
