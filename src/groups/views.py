from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404

from groups.forms import GroupForm
from groups.models import Group
from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, DeleteView, CreateView

from teachers.models import Teacher


class GetGroupView(LoginRequiredMixin, ListView):
    model = Group
    template_name = "group_get.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_group")
    context_object_name = "groups"
    queryset = Group.objects.all()
    login_url = "main:guest"


class CreateGroupView(LoginRequiredMixin, CreateView):
    template_name = "student-create.html"
    form_class = GroupForm
    success_url = reverse_lazy("students:get_student")
    login_url = "main:guest"


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    template_name = "teacher_update.html"
    form_class = GroupForm
    success_url = reverse_lazy("students:get_student")
    queryset = Group.objects.all()
    login_url = "main:guest"


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    model = Group
    template_name = "group_delete.html"
    success_url = reverse_lazy("groups:get_group")
    login_url = "main:guest"


class TeacherGroupListView(LoginRequiredMixin, ListView):
    template_name = "teacher_group_list_view.html"
    context_object_name = "groups"
    login_url = "main:guest"

    def get_queryset(self):
        teacher = get_object_or_404(Teacher, pk=self.kwargs.get("pk"))
        queryset = teacher.group.all()
        return queryset


class GroupTeacherListView(LoginRequiredMixin, ListView):
    template_name = "group_teacher_list_view.html"
    context_object_name = "teachers"
    login_url = "main:guest"

    def get_queryset(self):
        group = get_object_or_404(Group, pk=self.kwargs.get("pk"))
        queryset = group.teacher_set.all()
        return queryset
