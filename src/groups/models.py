import random

from django.db import models
from faker import Faker


class Group(models.Model):
    groups_name = models.CharField(max_length=120, null=True, blank=True)
    direction = models.CharField(max_length=120, null=True, blank=True)
    location_school = models.CharField(max_length=100, null=True, blank=True)
    price = models.PositiveSmallIntegerField(null=True, blank=True)

    class Meta:
        verbose_name = "Groups"
        verbose_name_plural = "All groups"
        ordering = ("groups_name",)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        direction_list = [
            "Programming",
            "Testing",
            "Management",
            "Marketing",
            "Design",
            "Сourses for schoolchildren",
        ]
        for _ in range(count):
            cls.objects.create(
                groups_name=faker.word(),
                direction=random.choice(direction_list),
                location_school=faker.address(),
                price=random.randint(10000, 30000),
            )

    def __str__(self):
        return f"{self.groups_name}".upper()
