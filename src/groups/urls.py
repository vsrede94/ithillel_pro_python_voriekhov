from django.urls import path

from groups.views import (
    GroupTeacherListView,
    CreateGroupView,
    UpdateGroupView,
    DeleteGroupView,
    TeacherGroupListView,
    GetGroupView,
)

app_name = "groups"

urlpatterns = [
    path("", GetGroupView.as_view(), name="get_group"),
    path("group_teacher_list_view<int:pk>/", GroupTeacherListView.as_view(), name="group_teacher_list_view"),
    path("teacher_group_list/<int:pk>/", TeacherGroupListView.as_view(), name="teacher_group_list_view"),
    path("create/", CreateGroupView.as_view(), name="create_group"),
    path("update/<int:pk>/", UpdateGroupView.as_view(), name="update_group"),
    path("delete/<int:pk>/", DeleteGroupView.as_view(), name="delete_group"),
]
