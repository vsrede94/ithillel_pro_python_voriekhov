# Generated by Django 4.2.2 on 2023-07-31 07:40

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import main.utils.validators
import uuid


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("groups", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Student",
            fields=[
                (
                    "first_name",
                    models.CharField(
                        blank=True,
                        max_length=120,
                        null=True,
                        validators=[
                            django.core.validators.MinLengthValidator(
                                2, message="incorrect"
                            ),
                            main.utils.validators.first_name_validator,
                        ],
                    ),
                ),
                ("last_name", models.CharField(blank=True, max_length=120, null=True)),
                ("email", models.EmailField(blank=True, max_length=150, null=True)),
                ("birth_date", models.DateField(blank=True, null=True)),
                (
                    "uuid",
                    models.UUIDField(
                        db_index=True,
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                        unique=True,
                    ),
                ),
                ("grade", models.PositiveSmallIntegerField(blank=True, null=True)),
                (
                    "avatar",
                    models.ImageField(
                        blank=True, null=True, upload_to="student/avatar_student/"
                    ),
                ),
                (
                    "resume",
                    models.FileField(
                        blank=True, null=True, upload_to="student/resume_student/"
                    ),
                ),
                (
                    "group",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="groups.group"
                    ),
                ),
            ],
            options={
                "verbose_name": "Student",
                "verbose_name_plural": "All students",
                "ordering": ("last_name",),
            },
        ),
    ]
