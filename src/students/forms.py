from django.forms import ModelForm

from students.models import Student


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = ["first_name", "last_name", "email", "group", "avatar", "resume"]
