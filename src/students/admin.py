from django.contrib import admin

from students.models import Student


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    ordering = ("first_name",)
    list_display = ("email", "first_name", "last_name", "group", "calculate_age", "group_name", "avatar", "resume")
    list_display_links = ("email", "first_name")
    list_filter = ("group",)
    date_hierarchy = "birth_date"
    search_fields = ("first_name", "last_name")
    fieldsets = (
        ("Personal info", {"fields": ("email", "first_name", "last_name")}),
        ("Additional info", {"classes": ("collapse",), "fields": ("resume", "avatar", "group")}),
    )
    list_per_page = 20

    def group_name(self, instance):
        if instance.group:
            return instance.group.groups_name
        return "-"
