from django.urls import path
from students.views import CreateStudentView, UpdateStudentView, GetStudentView, DeleteStudentView

app_name = "students"

urlpatterns = [
    path("", GetStudentView.as_view(), name="get_student"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:uuid>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<uuid:uuid>/", DeleteStudentView.as_view(), name="delete_student"),
]
