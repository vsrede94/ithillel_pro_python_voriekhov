from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.views.generic import CreateView, UpdateView, ListView, DeleteView

from students.forms import StudentForm
from students.models import Student
from django.urls import reverse_lazy


class GetStudentView(LoginRequiredMixin, ListView):
    model = Student
    template_name = "student_get.html"
    form_class = StudentForm
    pk_url_kwarg = "uuid"
    success_url = reverse_lazy("students:get_student")
    context_object_name = "students"
    login_url = "main:guest"

    def get_queryset(self):
        queryset = Student.objects.all()
        search = self.request.GET.get("searchField")
        search_fields = ["first_name", "last_name", "email"]
        if search:
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": search})
            queryset = queryset.filter(or_filter)
        return queryset


class CreateStudentView(LoginRequiredMixin, CreateView):
    template_name = "student-create.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_student")
    login_url = "main:guest"


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    template_name = "student_update.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_student")
    queryset = Student.objects.all()
    pk_url_kwarg = "uuid"
    login_url = "main:guest"


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    model = Student
    template_name = "student_delete.html"
    success_url = reverse_lazy("students:get_student")
    pk_url_kwarg = "uuid"
    login_url = "main:guest"
