import random
from uuid import uuid4

from django.db import models
from faker import Faker

from groups.models import Group
from main.models import Person


class Student(Person):
    uuid = models.UUIDField(default=uuid4, primary_key=True, unique=True, editable=False, db_index=True)
    group = models.ForeignKey(to="groups.Group", on_delete=models.CASCADE)
    grade = models.PositiveSmallIntegerField(null=True, blank=True)
    avatar = models.ImageField(upload_to="student/avatar_student/", null=True, blank=True)
    resume = models.FileField(upload_to="student/resume_student/", null=True, blank=True)

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "All students"
        ordering = ("last_name",)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=18, maximum_age=60),
                group=Group.objects.get(id=random.randint(1, 5)),
            )

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email}"
